FROM docker.io/rustlang/rust:nightly-slim
COPY ./target/release/timetracker /usr/local/bin/timetracker
CMD ["/usr/local/bin/timetracker"]
