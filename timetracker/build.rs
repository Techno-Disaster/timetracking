extern crate vergen;

use vergen::{vergen, Config};

fn main() {
    // Setup the flags, toggling off the 'SEMVER_FROM_CARGO_PKG' flag
    let mut config: Config = Config::default();
    *config.git_mut().rerun_on_head_change_mut() = true;

    // Generate the 'cargo:' key output
    vergen(config).expect("Unable to generate the cargo keys!");
}
