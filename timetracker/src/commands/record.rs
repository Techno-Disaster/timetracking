use crate::config::Config;
use crate::database::Database;
use crate::errors::Error;
use chrono::{DateTime, Utc};
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(
    help = "`!record <duration> <description>` - Records a task UNTIL now for the provided duration."
)]
pub async fn record<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    config: Config<'a>,
    sender: String,
    mut args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    if args.len() < 2 {
        tx.send_notice("⚠️ WARNING: You need to add a description at the end of this command. This Command was not saved therefore.".to_string(),None).await?;

        return Ok(());
    }
    let raw_duration = <&str>::clone(&args[0]);
    let std_duration = parse_duration::parse(raw_duration)?;
    let duration = chrono::Duration::from_std(std_duration)?;
    let minutes = duration.num_minutes();
    if minutes <= 1 {
        tx.send_notice(
            "⚠️ WARNING: The duration needs to be a positive amount that is greater than 1 minute."
                .to_string(),
            None,
        )
        .await?;

        return Ok(());
    }
    if minutes >= 24 * 60 {
        tx.send_notice(
            "⚠️ WARNING: The duration needs to be smaller than 24h.".to_string(),
            None,
        )
        .await?;

        return Ok(());
    }

    args.drain(0..1);
    let raw_description = args.join(" ");

    let database = Database::new(config).await?;

    let time_utc: DateTime<Utc> = Utc::now();
    let new_time = time_utc + duration;
    database
        .insert_full_time(sender, time_utc, new_time, raw_description)
        .await?;

    tx.send_notice(
        format!(
            "✅ Successfully saved \"duration\" from \"{}\" until \"{}\". Happy working! 🎉",
            time_utc.to_rfc3339(),
            new_time.to_rfc3339()
        ),
        None,
    )
    .await?;

    Ok(())
}
