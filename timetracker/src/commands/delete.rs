use crate::config::Config;
use crate::database::Database;
use crate::errors::Error;
use crate::utils::time_utils::{parse_times, parse_timezone_str};
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(
    help = "`!delete <in|out> <exact timestamp>` - Removes the time from either the in or out table."
)]
pub async fn delete<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    config: Config<'a>,
    sender: String,
    args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    if args.len() < 2 {
        tx.send_notice("🚨 ERROR: Missing required arguments!".to_string(), None)
            .await?;

        return Ok(());
    }
    let table = <&str>::clone(&args[0]);
    let raw_time = <&str>::clone(&args[1]);

    let database = Database::new(config).await?;
    let user_settings = database.get_user_settings(sender.clone()).await?;

    let time = parse_times(
        raw_time,
        parse_timezone_str(&user_settings.unwrap().timezone)?,
    )?;

    if table != "out" && table != "in" {
        tx.send_notice("⚠️ WARNING: https://xkcd.com/327/ Only use 'in' or 'out'. This Command was not executed therefore.".to_string(),None).await?;

        return Ok(());
    }

    // Can't do it using sq query vars apparently
    if table == "in" {
        database.delete_in(sender, time).await?;
    } else {
        database.delete_out(sender, time).await?;
    }
    tx.send_notice(
        "✅ Successfully removed the specified entry".to_string(),
        None,
    )
    .await?;

    Ok(())
}
