use crate::database::Database;
use crate::errors::Error;
use crate::utils::{
    html_templates,
    time_utils::{generate_filter_date_range, pretty_duration},
};
use crate::{config::Config, utils::time_utils::parse_timezone_str};
use askama::Template;
use chrono::{Duration, NaiveDateTime, TimeZone, Utc};
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(
    help = "`!stats [range]` - Defaults to a range of `day`. Possible values are: `day`, `week`, `month`, `lastmonth`."
)]
pub async fn stats<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    config: Config<'a>,
    sender: String,
    args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    if config.admins.iter().any(|x| *x == sender.clone()) {
        // Allow other users (one more arg)
        if args.len() > 2 {
            tx.send_notice(
                "Only 2 argument was expected. Please check your input".to_string(),
                None,
            )
            .await?;
            return Ok(());
        }
    } else if args.len() > 1 {
        tx.send_notice(
            "Only 1 argument was expected. Please check your input".to_string(),
            None,
        )
        .await?;
        return Ok(());
    }

    let (start_date, end_date) = if args.len() == 1 {
        generate_filter_date_range(args[0])?
    } else {
        generate_filter_date_range("day")?
    };

    let database = Database::new(config.clone()).await?;
    let user_settings = database.get_user_settings(sender.clone()).await?;
    let timezone = parse_timezone_str(&user_settings.clone().unwrap().timezone)?;
    let times = database.get_times(sender, start_date, end_date).await?;

    let sum: i64 = times
        .iter()
        .filter(|time| time.out.is_some())
        .map(|time| {
            let in_time = NaiveDateTime::from_timestamp(time.r#in, 0);
            let out_time = NaiveDateTime::from_timestamp(time.out.unwrap(), 0);

            out_time.signed_duration_since(in_time).num_minutes()
        })
        .sum();

    // Generate tables data
    let mut html_table_contents: Vec<Vec<String>> = times
        .iter()
        .map(|time| {
            let in_time = NaiveDateTime::from_timestamp(time.r#in, 0);
            let out_time = if time.out.is_some() {
                NaiveDateTime::from_timestamp(time.out.unwrap(), 0)
            } else {
                timezone
                    .from_local_datetime(&Utc::now().naive_local())
                    .single()
                    .ok_or(Error::NotATimeOrDate)
                    .unwrap()
                    .naive_local()
            };
            let description: String = time.description.clone().unwrap_or_default();
            let string_day = in_time.format("%d.%m.%Y").to_string();

            let duration = out_time.signed_duration_since(in_time);
            let duration_string = pretty_duration(duration);

            let html_row = vec![string_day, duration_string, description];

            html_row
        })
        .collect();

    html_table_contents.push(vec![
        "Sum".to_string(),
        pretty_duration(Duration::minutes(sum)),
    ]);

    // TODO fix thread safety issue

    /*let table = Table::new(
        Style::Simple,
        fallback_table_contents,
        Some(Headers::from(vec!["Day", "Hours"])),
    )
    .tabulate();*/

    tx.send_notice("Due to thread safety issues we cant do a fallback currently. Please use a client that renders HTML".to_string(),Some(html_templates::StatsMessageTemplate {
        rows: html_table_contents,
    }.render().unwrap())).await?;

    Ok(())
}
