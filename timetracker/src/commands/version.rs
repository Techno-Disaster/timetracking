use crate::config::Config;
use crate::errors::Error;
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(help = "`!version` - The current version used")]
pub async fn version<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    _config: Config<'a>,
    _sender: String,
    mut _args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    tx.send_notice(
        format!("Current Version: {}", env!("VERGEN_GIT_SEMVER")),
        None,
    )
    .await?;
    Ok(())
}
