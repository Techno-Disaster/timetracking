use crate::config::Config;
use crate::database::Database;
use crate::errors::Error;
use crate::utils::time_utils::{parse_times, parse_times_to_timezone, parse_timezone_str};
use chrono::{DateTime, Duration, TimeZone, Utc};
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(
    help = "`!out [dateTime] <description>` - Stops recording the time `now` or at the defined time. Description is required if no description was given on in."
)]
pub async fn out<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    config: Config<'a>,
    sender: String,
    mut args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    let database = Database::new(config.clone()).await?;
    let user_settings = database.get_user_settings(sender.clone()).await?;

    let timezone = parse_timezone_str(&user_settings.clone().unwrap().timezone)?;

    let last_times = database.get_last(sender).await?;

    if last_times.is_none() {
        tx.send_notice("⚠️ WARNING: You are still missing a !in. There is no correct previous in recorded. This Command was not saved therefore.".to_string(),None).await?;

        return Ok(());
    }
    return if let Some(last_times_db) = last_times {
        if last_times_db.out.is_some() {
            tx.send_notice("⚠️ WARNING: You are still missing a !in. There is no correct previous in recorded. This Command was not saved therefore.".to_string(),None).await?;

            return Ok(());
        }
        if last_times_db.description.is_none() && args.len() <= 1 {
            if args.is_empty() {
                tx.send_notice("⚠️ WARNING: You didn't add a description to your !in. You are required to set it now. This Command was not saved therefore.".to_string(),None).await?;

                return Ok(());
            }

            let raw_arg = <&str>::clone(&args[0]);

            let time = parse_times(raw_arg, timezone);

            // Decide if we got a description or a time
            if time.is_ok() {
                tx.send_notice("⚠️ WARNING: You didn't add a description to your !in. You are required to set it now. This Command was not saved therefore.".to_string(),None).await?;
                return Ok(());
            }
        }
        if !args.is_empty() {
            let raw_arg = <&str>::clone(&args[0]);
            let time = parse_times(raw_arg, timezone);

            match time {
                Ok(time) => {
                    let last_in_naive = Utc.timestamp(last_times_db.r#in, 0).naive_local();
                    let last_in = Utc.from_local_datetime(&last_in_naive).single().unwrap();
                    let time = Utc
                        .from_local_datetime(&time.naive_local())
                        .single()
                        .unwrap();
                    if time < last_in {
                        tx.send_notice(
                            "🚨 ERROR: Don't do this shine! (Time before last in is not allowed)"
                                .to_string(),
                            None,
                        )
                        .await?;

                        return Ok(());
                    }

                    let last_in_weektime = last_in + Duration::weeks(1);
                    if time >= last_in_weektime {
                        tx.send_notice(
                            "🚨 ERROR: The out time needs to be within 1 week of the in time"
                                .to_string(),
                            None,
                        )
                        .await?;

                        return Ok(());
                    }

                    if args.len() >= 2 {
                        args.drain(0..1);
                        let raw_description = args.join(" ");
                        database
                            .insert_out_time(last_times_db.r#in, time, Some(raw_description))
                            .await?;
                    } else {
                        database
                            .insert_out_time(last_times_db.r#in, time, None)
                            .await?;
                    }

                    tx.send_notice(
                        format!(
                            "✅ Successfully saved \"out\" time at {}. Happy free time! 🎉",
                            parse_times_to_timezone(raw_arg, timezone)?.to_rfc3339()
                        ),
                        None,
                    )
                    .await?;

                    Ok(())
                }
                Err(_) => {
                    let time_utc: DateTime<Utc> = DateTime::<Utc>::from_utc(
                        timezone
                            .from_local_datetime(&Utc::now().naive_local())
                            .single()
                            .ok_or(Error::NotATimeOrDate)?
                            .naive_local(),
                        Utc,
                    );
                    let time = time_utc.with_timezone(&timezone);

                    database
                        .insert_out_time(last_times_db.r#in, time_utc, Some(raw_arg.into()))
                        .await?;

                    tx.send_notice(
                        format!(
                            "✅ Successfully saved \"out\" time at {}. Happy free time! 🎉",
                            time.to_rfc3339()
                        ),
                        None,
                    )
                    .await?;

                    Ok(())
                }
            }
        } else {
            let time_utc: DateTime<Utc> = DateTime::<Utc>::from_utc(
                timezone
                    .from_local_datetime(&Utc::now().naive_local())
                    .single()
                    .ok_or(Error::NotATimeOrDate)?
                    .naive_local(),
                Utc,
            );
            let time = time_utc.with_timezone(&timezone);
            database
                .insert_out_time(last_times_db.r#in, time_utc, None)
                .await?;

            tx.send_notice(
                format!(
                    "✅ Successfully saved \"out\" time at {}. Happy free time! 🎉",
                    time.to_rfc3339()
                ),
                None,
            )
            .await?;

            Ok(())
        }
    } else {
        tx.send_notice("⚠️ WARNING: You are still missing a !in. There is no previous \"in\" recorded. This Command was not saved therefore.".to_string(),None).await?;

        Ok(())
    };
}
