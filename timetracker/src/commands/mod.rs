use crate::config::Config;
use crate::database::Database;
use crate::errors::Error;
use crate::utils::time_utils::parse_timezone_str;
use mrsbfh::commands::command_generate;

pub mod r#break;
pub mod delete;
pub mod r#in;
pub mod out;
pub mod record;
pub mod set_timezone_default;
pub mod stats;
pub mod version;

#[command_generate(
    bot_name = "Timetracking Matrix",
    description = "*Short commands (first character of each word if command was written as snail_case) are available for all commands*\n
*Arguments surrounded with [] are considered optional*\n
*Arguments surrounded with <> are considered required*\n
*Time and Dates must be defined in [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) formatting*"
)]
enum Commands {
    In,
    Out,
    Break,
    Delete,
    Record,
    SetTimezoneDefault,
    Stats,
    Version,
}

/// Check if we have a db entry and otherwise add it
// TODO might be optimizable by caching it in memory or making it a command inside of postgres itself
pub async fn ensure_user_exists_in_db(
    sender: String,
    sender_display_name: String,
    config: Config<'_>,
) -> Result<(), Error> {
    let database = Database::new(config.clone()).await?;
    let user = database.get_user(sender.clone()).await?;
    if user.is_none() {
        let default_timezone: &str = match config.default_timezone {
            Some(default_timezone) => parse_timezone_str(&default_timezone)?.name(),
            None => "UTC",
        };
        // Creates new user
        database
            .create_user(sender_display_name, sender.clone(), default_timezone.into())
            .await?;
    }

    let admin = config.admins.iter().any(|x| *x == sender);
    // Set correct admin status
    database.update_admin_status(sender, admin).await?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Config;
    use crate::database::Database;
    use dotenv::dotenv;
    use matrix_sdk::{
        events::{room::message::MessageEventContent, AnyMessageEventContent},
        Client,
    };
    use regex::Regex;
    use sqlx::migrate::MigrateDatabase;
    use sqlx::Postgres;
    use std::env;

    //TODO cleanup code

    pub(crate) async fn prepare() {
        dotenv().ok();
        if env::var("RUST_LOG").is_err() {
            env::set_var("RUST_LOG", "INFO");
        }
        reset_db().await;
    }

    pub(crate) fn get_config() -> Config<'static> {
        Config {
            homeserver_url: Default::default(),
            mxid: Default::default(),
            password: Default::default(),
            store_path: Default::default(),
            session_path: Default::default(),
            database_url: Default::default(), // Not needed for tests
            admins: vec!["test_user".into()],
            allowed_users: vec!["test_user".into()],
            default_timezone: Default::default(),
        }
    }

    pub(crate) fn get_channel() -> (
        tokio::sync::mpsc::Receiver<AnyMessageEventContent>,
        tokio::sync::mpsc::Sender<AnyMessageEventContent>,
    ) {
        let (tx, rx) = tokio::sync::mpsc::channel(100);

        (rx, tx)
    }

    async fn reset_db() {
        let database = Database::new(get_config()).await.unwrap();
        database.close().await;
        let url = &env::var("DATABASE_URL").unwrap();
        if Postgres::database_exists(url).await.unwrap() {
            Postgres::drop_database(url).await.unwrap();
        }
        Postgres::create_database(url).await.unwrap();

        ensure_user_exists_in_db("test_user".into(), "test_user".into(), get_config())
            .await
            .unwrap();
        ensure_user_exists_in_db("test_user2".into(), "test_user2".into(), get_config())
            .await
            .unwrap();
    }

    async fn dummy_client() -> Client {
        // This does nothing. Its just required to compile.
        Client::new("https://localhost").unwrap()
    }

    #[tokio::test]
    async fn simple_in_should_work() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command("in", client, get_config(), tx, "test_user".into(), vec![])
            .await
            .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }
    }

    #[tokio::test]
    async fn in_out_without_description_should_fail() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec![],
        )
        .await
        .unwrap();

        match_command("out", client, get_config(), tx, "test_user".into(), vec![])
            .await
            .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(notice
                .body
                .contains("⚠️ WARNING: You didn't add a description to your !in. You are required to set it now. This Command was not saved therefore."));
        }
    }

    #[tokio::test]
    async fn in_out_should_work() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec![],
        )
        .await
        .unwrap();

        match_command(
            "out",
            client,
            get_config(),
            tx,
            "test_user".into(),
            vec!["test"],
        )
        .await
        .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let out_expected = Regex::new("✅ Successfully saved \"out\" time at (.*).").unwrap();

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();

        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(out_expected.is_match(notice.body.as_str()));
        }
    }

    #[tokio::test]
    async fn in_out_with_2_users_and_same_times_should_work() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec!["17:00", "test"],
        )
        .await
        .unwrap();

        match_command(
            "out",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec!["18:00"],
        )
        .await
        .unwrap();

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user2".into(),
            vec!["17:00", "test"],
        )
        .await
        .unwrap();

        match_command(
            "out",
            client,
            get_config(),
            tx,
            "test_user2".into(),
            vec!["18:00"],
        )
        .await
        .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let out_expected = Regex::new("✅ Successfully saved \"out\" time at (.*).").unwrap();

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();

        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(out_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();

        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(out_expected.is_match(notice.body.as_str()));
        }
    }

    #[tokio::test]
    async fn in_out_should_work_with_future_times() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec!["16:00"],
        )
        .await
        .unwrap();

        match_command(
            "out",
            client,
            get_config(),
            tx,
            "test_user".into(),
            vec!["17:00", "test"],
        )
        .await
        .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let out_expected = Regex::new("✅ Successfully saved \"out\" time at (.*).").unwrap();
        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();

        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(out_expected.is_match(notice.body.as_str()));
        }
    }

    #[tokio::test]
    async fn in_in_should_work_when_description_set() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec!["test"],
        )
        .await
        .unwrap();

        match_command(
            "in",
            client,
            get_config(),
            tx,
            "test_user".into(),
            vec!["test"],
        )
        .await
        .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let out_expected = Regex::new("✅ Successfully saved \"out\" time at (.*).").unwrap();
        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(out_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }
    }

    #[tokio::test]
    async fn in_in_should_not_work_when_description_missing() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec![],
        )
        .await
        .unwrap();

        match_command(
            "in",
            client,
            get_config(),
            tx,
            "test_user".into(),
            vec!["test"],
        )
        .await
        .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(notice
                .body
                .contains("⚠️ WARNING: You did not set a description on your last !in. Please explicitly call !out. This Command was therefore not saved."));
        }
    }

    #[tokio::test]
    async fn record_should_work() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "record",
            client,
            get_config(),
            tx,
            "test_user".into(),
            vec!["10m", "test"],
        )
        .await
        .unwrap();

        let expected = Regex::new(
            "✅ Successfully saved \"duration\" from \".*\" until \".*\". Happy working! 🎉",
        )
        .unwrap();

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(expected.is_match(notice.body.as_str()));
        }
    }

    #[tokio::test]
    async fn out_without_in_should_fail() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command("out", client, get_config(), tx, "test_user".into(), vec![])
            .await
            .unwrap();

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(notice
                .body
                .contains("⚠️ WARNING: You are still missing a !in. There is no correct previous in recorded. This Command was not saved therefore."));
        }
    }

    #[tokio::test]
    async fn out_before_in_should_fail() {
        prepare().await;
        let (mut rx, tx) = get_channel();
        let client = dummy_client().await;

        match_command(
            "in",
            client.clone(),
            get_config(),
            tx.clone(),
            "test_user".into(),
            vec!["2020-11-06T10:30:05.520418300+00:00"],
        )
        .await
        .unwrap();

        match_command(
            "out",
            client,
            get_config(),
            tx,
            "test_user".into(),
            vec!["2020-11-04T10:30:05.520418300+00:00", "test"],
        )
        .await
        .unwrap();

        let in_expected =
            Regex::new("✅ Successfully saved \"in\" time at (.*). Happy work! 🎉").unwrap();
        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(in_expected.is_match(notice.body.as_str()));
        }

        let result = rx.recv().await.unwrap();
        if let AnyMessageEventContent::RoomMessage(MessageEventContent::Notice(notice)) = result {
            assert!(notice
                .body
                .contains("🚨 ERROR: Don't do this shine! (Time before last in is not allowed)"));
        }
    }
}
