use crate::config::Config;
use crate::database::Database;
use crate::errors::Error;
use crate::utils::time_utils::{parse_times, parse_times_to_timezone, parse_timezone_str};
use chrono::{DateTime, TimeZone, Utc};
use chrono_tz::Tz;
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(
    help = "`!in [dateTime] [description]` - Records the time from `now` or the defined time."
)]
pub async fn r#in<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    config: Config<'a>,
    sender: String,
    mut args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    let database = Database::new(config.clone()).await?;
    let last_times = database.get_last(sender.clone()).await?;

    if let Some(ref last_times_db) = last_times {
        if last_times_db.out.is_none() {
            if last_times_db.description.is_none() {
                tx.send_notice("⚠️ WARNING: You did not set a description on your last !in. Please explicitly call !out. This Command was therefore not saved.".to_string(),None).await?;
                return Ok(());
            }

            let time_utc: DateTime<Utc> = Utc::now();
            database.set_out(time_utc, last_times_db.r#in).await?;

            tx.send_notice(
                format!(
                    "✅ Successfully saved \"out\" time at {}.",
                    time_utc.to_rfc3339()
                ),
                None,
            )
            .await?;
        }
    }

    let user_settings = database.get_user_settings(sender.clone()).await?;
    let timezone = parse_timezone_str(&user_settings.clone().unwrap().timezone)?;
    return if !args.is_empty() {
        let raw_arg = <&str>::clone(&args[0]);

        let time = parse_times(raw_arg, timezone);
        match time {
            Ok(time) => {
                if let Some(last_times_db) = last_times {
                    if let Some(_last_out_db) = last_times_db.out {
                        let time = DateTime::<Utc>::from_utc(time.naive_local(), Utc);
                        let time_utc: DateTime<Utc> = DateTime::<Utc>::from_utc(
                            timezone
                                .from_local_datetime(&Utc::now().naive_local())
                                .single()
                                .ok_or(Error::NotATimeOrDate)?
                                .naive_local(),
                            Utc,
                        );
                        if time > time_utc {
                            tx.send_notice("🚨 ERROR: Don't do this shine! (Time after now is not allowed). This Command was not saved therefore.".to_string(),None).await?;

                            return Ok(());
                        }
                    }
                }

                if args.len() >= 2 {
                    args.drain(0..1);
                    let raw_description = args.join(" ");

                    database
                        .insert_in_time(sender, time, Some(raw_description))
                        .await?;
                } else {
                    database.insert_in_time(sender, time, None).await?;
                }
                tx.send_notice(
                    format!(
                        "✅ Successfully saved \"in\" time at {}. Happy work! 🎉",
                        parse_times_to_timezone(
                            raw_arg,
                            parse_timezone_str(&user_settings.unwrap().timezone)?,
                        )?
                        .to_rfc3339()
                    ),
                    None,
                )
                .await?;
                Ok(())
            }
            Err(_) => {
                let time_utc: DateTime<Utc> = DateTime::<Utc>::from_utc(
                    timezone
                        .from_local_datetime(&Utc::now().naive_local())
                        .single()
                        .ok_or(Error::NotATimeOrDate)?
                        .naive_local(),
                    Utc,
                );
                let time = time_utc.with_timezone(&timezone);

                // Join args to a string
                let description = args.join(" ");

                database
                    .insert_in_time(sender, time_utc, Some(description))
                    .await?;

                tx.send_notice(
                    format!(
                        "✅ Successfully saved \"in\" time at {}. Happy work! 🎉",
                        time.to_rfc3339()
                    ),
                    None,
                )
                .await?;

                Ok(())
            }
        }
    } else {
        let time_utc: DateTime<Utc> = DateTime::<Utc>::from_utc(
            timezone
                .from_local_datetime(&Utc::now().naive_local())
                .single()
                .ok_or(Error::NotATimeOrDate)?
                .naive_local(),
            Utc,
        );
        let time: DateTime<Tz> = time_utc.with_timezone(&timezone);
        database.insert_in_time(sender, time_utc, None).await?;

        tx.send_notice(
            format!(
                "✅ Successfully saved \"in\" time at {}. Happy work! 🎉",
                time.to_rfc3339()
            ),
            None,
        )
        .await?;

        Ok(())
    };
}
