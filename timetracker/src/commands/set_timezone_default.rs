use crate::config::Config;
use crate::database::Database;
use crate::errors::Error;
use crate::utils::time_utils::parse_timezone_str;
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(
    help = "`!setTimezoneDefault <timezone or UTC offset>` - Sets the default timezone in which times are given."
)]
pub async fn set_timezone_default<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    config: Config<'a>,
    sender: String,
    args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    let timezone = parse_timezone_str(args[0])?;
    let database = Database::new(config).await?;
    database
        .set_timezone(sender, timezone.name().into())
        .await?;

    tx.send_notice(
        format!(
            "✅ Successfully saved default Timezone \"{}\". 🎉",
            timezone.name(),
        ),
        None,
    )
    .await?;

    Ok(())
}
