use crate::config::Config;
use crate::errors::Error;
use matrix_sdk::Client;
use mrsbfh::{commands::command, MatrixMessageExt};

#[command(help = "`!break <start|stop>` - Starts or stops a break.")]
pub async fn r#break<'a>(
    _client: Client,
    mut tx: mrsbfh::Sender,
    _config: Config<'a>,
    _sender: String,
    mut _args: Vec<&str>,
) -> Result<(), Error>
where
    Config<'a>: mrsbfh::config::Loader + Clone,
{
    tx.send_notice(
        "❌ ERROR: Sorry this is not yet implemented".to_string(),
        None,
    )
    .await?;

    // TODO check if we are in either state and warn user about it
    //let raw_action = args[0];
    /*if raw_action == "start" {
    } else {
    }*/
    Ok(())
}
