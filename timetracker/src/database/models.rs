#[derive(Debug, Clone)]
pub struct User {
    pub mxid: String,
    pub display_name: String,
}

#[derive(Debug)]
pub struct Time {
    pub mxid: String,
    pub r#in: i64,
    pub out: Option<i64>,
    pub description: Option<String>,
}

#[derive(Debug, Clone)]
pub struct Setting {
    pub mxid: String,
    pub timezone: String,
    pub admin: bool,
}
