use crate::config::Config;
use crate::database::models::{Setting, Time, User};
use crate::errors::Error;
use chrono::{DateTime, NaiveDate, Utc};
use include_dir::{include_dir, Dir};
use sqlx::postgres::PgPool;
use sqlx_pg_migrate::migrate;
use tracing::{instrument, warn};

pub mod models;

// The path here is relative to the cargo root.
static MIGRATIONS: Dir = include_dir!("migrations");

/// The general struct to access the Database
#[derive(Clone, Debug)]
pub struct Database {
    pool: PgPool,
}

impl Database {
    #[cfg(test)]
    #[instrument(skip(_config))]
    pub async fn new<'a>(_config: Config<'a>) -> Result<Self, Error>
    where
        Config<'a>: mrsbfh::config::Loader + Clone,
    {
        use std::env;
        if let Err(e) = migrate(&env::var("DATABASE_URL")?, &MIGRATIONS).await {
            warn!("Database Error: {}", e);
        }
        Ok(Self {
            pool: PgPool::connect(&env::var("DATABASE_URL")?).await?,
        })
    }

    // This is a singleton
    #[cfg(not(test))]
    #[instrument(skip(config))]
    pub async fn new<'a>(config: Config<'a>) -> Result<Self, Error>
    where
        Config<'a>: mrsbfh::config::Loader + Clone,
    {
        use once_cell::sync::OnceCell;
        static INSTANCE: OnceCell<Database> = OnceCell::new();
        if let Some(pool) = INSTANCE.get() {
            Ok(pool.clone())
        } else {
            if let Err(e) = migrate(&config.database_url, &MIGRATIONS).await {
                warn!("Database Error: {}", e);
            }
            let pool = PgPool::connect(&config.database_url).await?;
            if INSTANCE.set(Self { pool }).is_err() {
                return Err(Error::DatabaseSingletonError);
            }
            Ok(INSTANCE.get().unwrap().clone())
        }
    }

    #[cfg(test)]
    pub async fn close(&self) {
        self.pool.close().await
    }

    // Getters
    pub async fn get_last(&self, sender: String) -> Result<Option<Time>, Error> {
        Ok(sqlx::query_as!(
            Time,
            r#"SELECT * from times where mxid = $1 ORDER BY "out" DESC limit 1;"#,
            sender
        )
        .fetch_optional(&self.pool)
        .await?)
    }

    pub async fn get_user(&self, sender: String) -> Result<Option<User>, Error> {
        Ok(
            sqlx::query_as!(User, "SELECT * from users where mxid = $1", sender)
                .fetch_optional(&self.pool)
                .await?,
        )
    }

    pub async fn get_user_settings(&self, sender: String) -> Result<Option<Setting>, Error> {
        Ok(
            sqlx::query_as!(Setting, "SELECT * from settings where mxid = $1", sender)
                .fetch_optional(&self.pool)
                .await?,
        )
    }

    pub async fn get_times(
        &self,
        sender: String,
        start_date: NaiveDate,
        end_date: NaiveDate,
    ) -> Result<Vec<Time>, Error> {
        Ok(
            sqlx::query_as!(Time,
                r#"SELECT * FROM times WHERE mxid = $1 AND ("in" BETWEEN EXTRACT(EPOCH FROM $2::date) AND EXTRACT(EPOCH FROM $3::date)) ORDER BY "in" ASC"#,
                sender,
                start_date,
                end_date,
            )
                .fetch_all(&self.pool)
                .await?,
        )
    }

    // Setters

    ///Creates user and sets default settings
    pub async fn create_user(
        &self,
        display_name: String,
        sender: String,
        default_timezone: String,
    ) -> Result<(), Error> {
        // Create user
        sqlx::query!(
            r#"
                    INSERT INTO users ( display_name, mxid )
                    VALUES ( $1, $2 )
                "#,
            display_name,
            sender
        )
        .execute(&self.pool)
        .await?;

        sqlx::query!(
            r#"INSERT INTO settings ( mxid, timezone, admin )
               VALUES ( $1, $2, false )
            "#,
            sender,
            default_timezone
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn update_admin_status(&self, sender: String, admin: bool) -> Result<(), Error> {
        // Set correct admin status
        sqlx::query!(
            r#"
                UPDATE settings SET admin = $2 WHERE mxid = $1;
            "#,
            sender,
            admin
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn set_out(&self, time_utc: DateTime<Utc>, r#in: i64) -> Result<(), Error> {
        sqlx::query!(
            r#"UPDATE times SET out = $1 WHERE "in" = $2"#,
            time_utc.timestamp(),
            r#in
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn set_timezone(&self, sender: String, timezone: String) -> Result<(), Error> {
        sqlx::query!(
            r#"UPDATE settings SET timezone = $1 WHERE mxid = $2;"#,
            timezone,
            sender,
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn insert_in_time(
        &self,
        sender: String,
        time: DateTime<Utc>,
        description: Option<String>,
    ) -> Result<(), Error> {
        if let Some(description) = description {
            sqlx::query!(
                r#"
                            INSERT INTO times ( mxid, "in", description )
                            VALUES ( $1, $2, $3 )
                        "#,
                sender,
                time.timestamp(),
                description
            )
            .execute(&self.pool)
            .await?;
        } else {
            sqlx::query!(
                r#"
                            INSERT INTO times ( mxid, "in" )
                            VALUES ( $1, $2 )
                        "#,
                sender,
                time.timestamp(),
            )
            .execute(&self.pool)
            .await?;
        }
        Ok(())
    }

    pub async fn insert_out_time(
        &self,
        in_time: i64,
        time: DateTime<Utc>,
        description: Option<String>,
    ) -> Result<(), Error> {
        if let Some(description) = description {
            sqlx::query!(
                r#"UPDATE times SET out = $1, description = $2 WHERE "in" = $3"#,
                time.timestamp(),
                description,
                in_time,
            )
            .execute(&self.pool)
            .await?;
        } else {
            sqlx::query!(
                r#"UPDATE times SET out = $1 WHERE "in" = $2"#,
                time.timestamp(),
                in_time,
            )
            .execute(&self.pool)
            .await?;
        }
        Ok(())
    }

    pub async fn insert_full_time(
        &self,
        sender: String,
        in_time: DateTime<Utc>,
        out_time: DateTime<Utc>,
        description: String,
    ) -> Result<(), Error> {
        sqlx::query!(
            r#"INSERT INTO times ( mxid, "in", out, description )
               VALUES ( $1, $2, $3, $4 )
            "#,
            sender,
            in_time.timestamp(),
            out_time.timestamp(),
            description,
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn delete_in(&self, sender: String, time: DateTime<Utc>) -> Result<(), Error> {
        sqlx::query!(
            r#"DELETE FROM times where "in" = $1 AND mxid = $2"#,
            time.timestamp(),
            sender,
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn delete_out(&self, sender: String, time: DateTime<Utc>) -> Result<(), Error> {
        sqlx::query!(
            r#"UPDATE times SET out = NULL WHERE out = $1 AND mxid = $2"#,
            time.timestamp(),
            sender,
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }
}
