use crate::database::models::Time;

/// The Interface for exporters.
///
/// As long as no crashes are caused all methods except help are optional.
///
pub trait Exporter {
    /// A brief helptext describing what the exporter does and how it works.
    fn help();

    /// Some action that doesn't need any of the actual values.
    fn simple_execute();

    /// Any in timestamp and its description. It may be empty when the description gets added on
    /// the out command.
    fn on_in(time: String, description: Option<String>);

    /// Any out timestamp and its description. It may be empty when the description already was in
    /// the in command.
    fn on_out(time: String, description: Option<String>);

    /// A action triggered on each out including the full database struct
    fn complete_out(db_entry: Time);

    /// Allow for creating or registering users if necessary. Equal to the ensure_user_exists_in_db
    /// function in the command_parser module.
    fn create_user(sender: String, sender_display_name: String);
}
