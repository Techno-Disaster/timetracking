#![forbid(unsafe_code)]
use crate::config::Config;
use clap::Clap;
use color_eyre::eyre::Result;
use database::Database;
use dotenv::dotenv;
use mrsbfh::config::Loader;
use tracing::{info, instrument};

mod commands;
mod config;
mod database;
mod errors;
mod exporters;
mod matrix;
mod utils;

#[derive(Clap)]
struct Opts {
    #[clap(short, long, default_value = "config.yml")]
    config: String,
}

#[instrument]
#[tokio::main]
pub async fn main() -> Result<()> {
    color_eyre::install()?;
    dotenv().ok();
    tracing_subscriber::fmt()
        .pretty()
        .with_thread_names(true)
        .with_max_level(tracing::Level::INFO)
        .init();
    info!("Starting...");
    let opts: Opts = Opts::parse();

    info!("Loading Configs...");
    let config = Config::load(opts.config)?;

    info!("Initialize Database");
    let _ = Database::new(config.clone());

    info!("Setting up Client...");
    let client = &mut matrix::setup(config.clone()).await?;
    info!("Starting Sync...");
    matrix::start_sync(client, config).await?;

    Ok(())
}
