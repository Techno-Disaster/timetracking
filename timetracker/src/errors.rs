use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("parsed string is not a command")]
    NotACommand,
    #[error("user is not allowed to use the bot")]
    NotAllowed,
    #[error(transparent)]
    DatabaseError(#[from] sqlx::Error),
    #[error(transparent)]
    ChronoError(#[from] chrono::ParseError),
    #[error(transparent)]
    EnvError(#[from] std::env::VarError),
    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),
    #[error("Unable to parse timezone")]
    TimezoneParseError,
    #[error("The string is no known type of time or date")]
    NotATimeOrDate,
    #[error(transparent)]
    DurationParseError(#[from] parse_duration::parse::Error),
    #[error(transparent)]
    DurationParseOutOfRangeError(#[from] time::OutOfRangeError),
    #[error(transparent)]
    TokioSendError(
        #[from] tokio::sync::mpsc::error::SendError<matrix_sdk::events::AnyMessageEventContent>,
    ),
    #[error("Custom ranges are not yet supported")]
    CustomRange,

    #[cfg(not(test))]
    #[error("Unable to set database singleton")]
    DatabaseSingletonError,
}
