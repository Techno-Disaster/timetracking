use askama::Template;

#[derive(Template)]
#[template(path = "messages/stats.html")]
pub struct StatsMessageTemplate {
    pub(crate) rows: Vec<Vec<String>>,
}
