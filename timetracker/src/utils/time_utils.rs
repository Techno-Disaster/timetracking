use crate::errors::Error;
use crate::errors::Error::TimezoneParseError;
use chrono::offset::LocalResult::Single;
use chrono::{
    DateTime, Datelike, Duration, NaiveDate, NaiveDateTime, NaiveTime, TimeZone, Utc, Weekday,
};
use chrono_tz::Tz;
use std::str::FromStr;

pub fn parse_times(time_str: &str, timezone: Tz) -> Result<DateTime<Utc>, Error> {
    let datetime = parse_times_to_timezone(time_str, timezone)?;
    Ok(DateTime::<Utc>::from_utc(datetime.naive_local(), Utc))
}

/// This tries multiple ways to parse the string as a time
/// It first checks for the RFC3339 format
/// Second for RFC2822
/// Third for Date + Time + Timezone (other or non-standard)
/// Fourth for Date + Time
/// Fifth Date
/// Last but not least Time
pub fn parse_times_to_timezone(time_str: &str, timezone: Tz) -> Result<DateTime<Tz>, Error> {
    if let Ok(time) = DateTime::parse_from_rfc3339(&time_str) {
        return timezone
            .from_local_datetime(&time.naive_local())
            .single()
            .ok_or(Error::NotATimeOrDate);
    };

    /*
    // TODO fixme Disabled as we cant have spaces anyway

    if let Ok(time) = DateTime::parse_from_rfc2822(&time_str) {
        return Ok(DateTime::<Utc>::from_utc(time.naive_local(), Utc));
    };

    if let Ok(time) = DateTime::parse_from_str(&time_str, "%Y-%m-%d %H:%M:%S %z") {
        return Ok(DateTime::<Utc>::from_utc(time.naive_local(), Utc));
    };
    if let Ok(time) = timezone.datetime_from_str(&time_str, "%Y-%m-%d %H:%M:%S") {
        return Ok(DateTime::<Utc>::from_utc(time.naive_local(), Utc));
    };*/
    if let Ok(time) = timezone.datetime_from_str(&time_str, "%Y-%m-%d") {
        return Ok(time);
    };
    if let Ok(time) = NaiveTime::parse_from_str(&time_str, "%H:%M:%S") {
        let naive_date = Utc::today().naive_utc();
        let naive_datetime = NaiveDateTime::new(naive_date, time);
        return if let Single(date_time_timezone) = timezone.from_local_datetime(&naive_datetime) {
            Ok(date_time_timezone)
        } else {
            Err(Error::NotATimeOrDate)
        };
    };
    if let Ok(time) = NaiveTime::parse_from_str(&time_str, "%H:%M") {
        let naive_date = Utc::today().naive_utc();
        let naive_datetime = NaiveDateTime::new(naive_date, time);
        return if let Single(date_time_timezone) = timezone.from_local_datetime(&naive_datetime) {
            Ok(date_time_timezone)
        } else {
            Err(Error::NotATimeOrDate)
        };
    }
    Err(Error::NotATimeOrDate)
}

pub fn generate_filter_date_range(range_str: &str) -> Result<(NaiveDate, NaiveDate), Error> {
    match range_str {
        "day" => {
            let today = Utc::today().naive_utc();
            let tomorrow = today + Duration::days(1);
            Ok((today, tomorrow))
        }
        "week" => {
            let today = Utc::today();
            let current_year = Utc::now().year();
            // We add 1 as apparently we get last year for not known reasons
            let this_iso_week = today.iso_week().week0() + 1;
            let monday = NaiveDate::from_isoywd(current_year, this_iso_week, Weekday::Mon);
            let sunday = NaiveDate::from_isoywd(current_year, this_iso_week, Weekday::Sun);
            Ok((monday, sunday))
        }
        "month" => {
            let now: DateTime<Utc> = Utc::now();
            let month_start = NaiveDate::from_ymd(now.year(), now.month(), 1);
            let month_end = NaiveDate::from_ymd_opt(now.year(), now.month() + 1, 1)
                .unwrap_or_else(|| NaiveDate::from_ymd(now.year() + 1, 1, 1))
                .pred();
            Ok((month_start, month_end))
        }
        "lastmonth" => {
            let now: DateTime<Utc> = Utc::now();
            let month_start = NaiveDate::from_ymd(now.year(), now.month() - 1, 1);
            let month_end = NaiveDate::from_ymd_opt(now.year(), (now.month() - 1) + 1, 1)
                .unwrap_or_else(|| NaiveDate::from_ymd(now.year() + 1, 1, 1))
                .pred();
            Ok((month_start, month_end))
        }
        // TODO custom range
        &_ => Err(Error::CustomRange),
    }
}

pub fn parse_timezone_str(timezone: &str) -> Result<Tz, Error> {
    // handle int based

    // The returned "Error" is of type String which does not implement String
    match Tz::from_str(timezone) {
        Ok(v) => Ok(v),
        Err(_) => Err(TimezoneParseError),
    }
}

pub fn pretty_duration(dur: Duration) -> String {
    let minutes = dur.num_minutes();
    format!("{:02}h{:02}m", minutes / 60, minutes % 60)
}
