use crate::commands::{ensure_user_exists_in_db, match_command};
use crate::errors::Error;
use crate::matrix::extensions::RoomExt;
use crate::Config;
use matrix_sdk::{
    async_trait,
    events::{
        room::member::MemberEventContent, room::message::MessageEventContent,
        AnyMessageEventContent, StrippedStateEvent, SyncMessageEvent,
    },
    identifiers::RoomId,
    Client, EventEmitter, SyncRoom,
};
use tokio::sync::mpsc;
use tracing::*;

/// TTMBot stands for `TimeTrackingMatrixBot`
#[derive(Debug, Clone)]
pub struct TTMBot {
    /// This clone of the `Client` will send requests to the server,
    /// while the other keeps us in sync with the server using `sync_forever`.
    client: Client,
    config: Config<'static>,
}

impl TTMBot {
    #[instrument]
    pub fn new(client: Client, config: Config<'static>) -> Self {
        Self {
            client,
            config: config.clone(),
        }
    }
}

impl TTMBot {
    async fn report_error(&self, e: Error, room_id: &RoomId) {
        if e.to_string() == Error::NotACommand.to_string()
            || e.to_string() == Error::NotAllowed.to_string()
        {
            // Ignore
            return;
        }
        if let Err(e) =
            self.client
                .room_send(
                    room_id,
                    AnyMessageEventContent::RoomMessage(MessageEventContent::notice_plain(
                        format!("Error happened: {}", e.to_string()),
                    )),
                    None,
                )
                .await
        {
            error!("{}", e);
        }
    }
}

#[mrsbfh::commands::commands]
#[mrsbfh::utils::autojoin]
#[async_trait]
impl EventEmitter for TTMBot {
    #[instrument(skip(self))]
    async fn on_room_message(&self, room: SyncRoom, event: &SyncMessageEvent<MessageEventContent>) {
        if let SyncRoom::Joined(ref room) = room {
            let sender = event.sender.clone().to_string();

            let room_id = room.read().await.clone().room_id;

            let display_name = room
                .read()
                .await
                .clone()
                .get_sender_displayname(event)
                .to_string();
            if let Err(e) = ensure_user_exists_in_db(
                event.sender.to_string(),
                display_name,
                self.config.clone(),
            )
            .await
            {
                error!("{}", e);
                self.report_error(e, &room_id).await;
            }

            // Ignore not allowed users
            if !self
                .config
                .allowed_users
                .iter()
                .any(|x| *x == sender.clone())
            {
                return;
            }
        }
    }

    // This does the autojoin for us
    #[instrument(skip(self))]
    async fn on_stripped_state_member(
        &self,
        room: SyncRoom,
        room_member: &StrippedStateEvent<MemberEventContent>,
        _: Option<MemberEventContent>,
    ) {
        if room_member.state_key != self.client.user_id().await.unwrap() {
            warn!("Got invite that isn't for us");
            return;
        }
        if let SyncRoom::Invited(_) = room {
            // Ignore not allowed users
            if !self
                .config
                .allowed_users
                .iter()
                .any(|x| *x == room_member.sender.to_string())
            {
                warn!("Invalid user tried to invite the bot");
                return;
            }
        }
    }
}
