use crate::Config;
use color_eyre::eyre::Result;
use matrix_sdk::{self, Client, ClientConfig, Session as SDKSession, SyncSettings};
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::fs;
use std::path::{Path, PathBuf};
use tracing::*;
use url::Url;

pub mod extensions;
mod sync;

#[instrument(skip(config))]
pub async fn setup(config: Config<'_>) -> Result<Client> {
    info!("Beginning Matrix Setup");
    let store_path_string = config.store_path.to_string();
    let store_path = Path::new(&store_path_string);
    if !store_path.exists() {
        fs::create_dir_all(store_path)?;
    }
    let client_config = ClientConfig::new().store_path(fs::canonicalize(&store_path)?);

    let homeserver_url =
        Url::parse(&config.homeserver_url).expect("Couldn't parse the homeserver URL");

    let client = Client::new_with_config(homeserver_url, client_config).unwrap();

    if let Some(session) = Session::load(config.clone()) {
        info!("Starting relogin");

        let session = SDKSession {
            access_token: session.access_token,
            device_id: session.device_id.into(),
            user_id: matrix_sdk::identifiers::UserId::try_from(session.user_id.as_str()).unwrap(),
        };

        if let Err(e) = client.restore_login(session).await {
            error!("{}", e);
        };
        info!("Finished relogin");
    } else {
        info!("Starting login");
        let login_response = client
            .login(
                &config.mxid,
                &config.password,
                None,
                Some(&"timetracking-bot".to_string()),
            )
            .await;
        match login_response {
            Ok(login_response) => {
                info!("Session: {:#?}", login_response);
                let session = Session {
                    homeserver: client.homeserver().to_string(),
                    user_id: login_response.user_id.to_string(),
                    access_token: login_response.access_token,
                    device_id: login_response.device_id.into(),
                };
                session.save(config.clone())?;
            }
            Err(e) => error!("Error while login: {}", e),
        }
        info!("Finished login");
    }

    info!("logged in as {}", config.mxid);

    Ok(client)
}

#[instrument(skip(client, config))]
pub async fn start_sync(client: &mut Client, config: Config<'static>) -> Result<()> {
    client
        .add_event_emitter(Box::new(sync::TTMBot::new(client.clone(), config.clone())))
        .await;

    info!("Starting full Sync...");
    client.sync(SyncSettings::default()).await;

    Ok(())
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Session {
    /// The homeserver used for this session.
    pub homeserver: String,
    /// The access token used for this session.
    pub access_token: String,
    /// The user the access token was issued for.
    pub user_id: String,
    /// The ID of the client device
    pub device_id: String,
}

impl Session {
    pub fn save(&self, config: Config) -> Result<()> {
        let mut session_path = PathBuf::from(config.session_path.to_string());
        info!("SessionPath: {:?}", session_path);
        std::fs::create_dir_all(&session_path)?;
        session_path.push("session.json");
        serde_json::to_writer(&std::fs::File::create(session_path)?, self)?;
        Ok(())
    }

    pub fn load(config: Config) -> Option<Self> {
        let mut session_path = PathBuf::from(config.session_path.to_string());
        session_path.push("session.json");
        let file = std::fs::File::open(session_path);
        match file {
            Ok(file) => {
                let session: Result<Self, serde_json::Error> = serde_json::from_reader(&file);
                match session {
                    Ok(session) => Some(session),
                    Err(_) => None,
                }
            }
            Err(_) => None,
        }
    }
}
