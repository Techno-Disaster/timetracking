CREATE TABLE IF NOT EXISTS settings (
    "mxid" varchar PRIMARY KEY NOT NULL,
    "timezone" varchar NOT NULL,
    "admin" boolean NOT NULL
);

ALTER TABLE settings ADD FOREIGN KEY ("mxid") REFERENCES users ("mxid");
