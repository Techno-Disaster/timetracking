CREATE TABLE IF NOT EXISTS times_backup AS
SELECT *
FROM times;

DROP TABLE times;

CREATE TABLE IF NOT EXISTS times
(
    "mxid"        varchar NOT NULL,
    "in"          bigint  NOT NULL PRIMARY KEY NOT NULL,
    "out"         bigint,
    "description" varchar
);

INSERT INTO times(mxid, "in", out, description)
SELECT mxid, EXTRACT(EPOCH FROM "in"::timestamptz) AS in, EXTRACT(EPOCH FROM "out"::timestamptz) AS out, description
FROM times_backup
ORDER BY id;