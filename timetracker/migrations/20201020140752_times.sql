CREATE TABLE IF NOT EXISTS times (
    "id" serial, -- Used for ordering
    "mxid" varchar NOT NULL,
    "in" varchar NOT NULL PRIMARY KEY,
    "out" varchar,
    "description" varchar
);

ALTER TABLE times ADD FOREIGN KEY ("mxid") REFERENCES users ("mxid");
