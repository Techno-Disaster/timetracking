CREATE TABLE IF NOT EXISTS users (
     "mxid" varchar PRIMARY KEY NOT NULL,
     "display_name" varchar NOT NULL
);
