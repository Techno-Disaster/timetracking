[![Matrix][matrix-shield]][matrix-url]
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/famedly/bots/timetracking">
    <img src="images/famedly.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Timetracking-Bot by Famedly</h3>

  <p align="center">
    A Bot with the function of tracking worktimes and providing exports for admins.
    <br />
    <!-- TODO <a href="https://gitlab.com/famedly/bots/timetracking"><strong>Explore the docs »</strong></a>-->
    <br />
    <br />
    <a href="https://gitlab.com/famedly/bots/timetracking/:q-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/famedly/bots/timetracking/-/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
    * [Built With](#built-with)
* [Getting Started](#getting-started)
    * [Prerequisites](#prerequisites)
    * [Installation](#installation)
        * [Ansible](#ansible)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project

The timetracking bot tracks the worktime at famedly.
It provides on the one side a bot frontend for the employee to log in and out and for admins
a way of exporting to csv.

Other features are: Postgres used as datastore, bot only responds to users that are allowed to 
use it, admins can be set in the config


### Built With

* [Rust](https://www.rust-lang.org/)
* [Matrix-Rust-SDK](https://github.com/matrix-org/matrix-rust-sdk/)
* [Matrix](https://matrix.org)
* [PostgreSQL](https://www.postgresql.org/)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* rust
```
Follow https://rustup.rs/ guides
```
* SQLX-Cli
```sh
cargo install --version=0.1.0-beta.1 sqlx-cli --no-default-features --features postgres
```

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/famedly/bots/timetracking.git
```

2. Create user and database in postgres
```sh
$ sudo -u postgres psql
postgres=# create user <USER> with encrypted password '<PASSWORD>';
postgres=# create database <DATABASE_NAME>;
postgres=# grant all privileges on database <DATABASE_NAME> to <USER>;
```

3. Copy the `.env.example` to `.env` and modify the content according to the above values.

4. Run migrations before first run (optional)
```sh
sqlx migrate run
```

5. Copy the `example.config.yml` to `config.yml` and fill in the values according to your setup

#### Ansible

An ansible role is available at https://gitlab.com/famedly/ansible/collections/matrix/-/tree/main/roles/timetracking-bot
and in the [Matrix Roles Collection](https://galaxy.ansible.com/famedly/matrix) by famedly.

<!-- USAGE EXAMPLES -->
## Usage
### Commands


*Short commands (first character) are available for all commands except `setTimezoneDefault`*


*Arguments surrounded with [] are considered optional*


*Arguments surrounded with <> are considered required*


*Time and Dates must be defined in [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) formatting*


*Durations must be defined in Minutes!*


* `!help` - This output
* `!in [dateTime] [description]` - Records the time from `now` or the defined time.
* `!out [dateTime] <description>` - Stops recording the time `now` or at the defined time. Description is required if no description was given on in.
* `!record <duration> <description>` - Records a task UNTIL now for the provided duration.
* `!break <start|stop>` - Starts or stops a break.
* `!delete <in|out> <exact timestamp>` - Removes the time from either the in or out table
* `!setTimezoneDefault <timezone or UTC offset>` - Sets the default timezone in which times are given.
* `!stats [range]` - Defaults to a range of `day`. Possible values are: `day`, `week`, `month`, `lastmonth`


<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/famedly/bots/timetracking/-/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


<!-- LICENSE -->
## License

Distributed under the AGPLv3 License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.com/famedly/bots/timetracking](https://gitlab.com/famedly/bots/timetracking)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[matrix-shield]: https://img.shields.io/matrix/timetracking-bot:famedly.de?label=Timetracking-Bot&style=flat-square
[matrix-url]: https://matrix.to/#/#timetracking-bot:famedly.de