# v0.2.0 to v0.3.0

- StateStore and CryptStore need to get deleted (The folder defined as `store_path`)
- `session_path` Needs to get added to the config. This needs to be a folder and keeps track of the access_token.
